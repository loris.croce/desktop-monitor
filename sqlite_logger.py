#!/usr/bin/env python3

# from : https://stackoverflow.com/questions/15381526/entering-data-into-sqlite

import serial
import time
from glob import glob
import sqlite3
import os
from datetime import datetime

db_filename = "sensorkit.db"

conn = sqlite3.connect(db_filename)

cursor = conn.cursor()
sql_file = os.path.join(os.path.dirname(__file__), db_filename)

needs_creation = not os.path.exists(sql_file)
db_connection = sqlite3.connect(sql_file)
# db_connection.row_factory = sqlite3.Row

# create table
if needs_creation:
    print("Creating database...")
    cursor = db_connection.cursor()
    db_connection.commit()
    print("Database created")

cursor.executescript(
    """
    DROP TABLE IF EXISTS sensors_data;
    CREATE TABLE sensors_data (id INTEGER PRIMARY KEY AUTOINCREMENT, light INTEGER, temperature REAL, humidity REAL, date DATE)
    """
)

# entering device file
arddev = glob("/dev/ttyACM*")[0]
baud = 115200

# Setup - if a Serial object can't be created, a SerialException will be raised
while True:
    try:
        ser = serial.Serial(arddev, baud)
        # break out of while loop when connection is made
        break
    except serial.SerialException:
        print("Waiting for device " + arddev + "to be available")
        time.sleep(3)

print("Logging sensors data...")


while not ser.in_waiting:
    csv_line = ser.readline().decode("ascii").strip("\n").strip("\r")
    csv_line = csv_line + "," + str(datetime.now())
    query = "INSERT INTO sensors_data(light, temperature, humidity, date) values(?, ?, ?, ?)"
    values = tuple(csv_line.split(","))
    # print(values)
    try:
        cursor.execute(query, values)
        db_connection.commit()
    except:
        continue
    conn.commit()
