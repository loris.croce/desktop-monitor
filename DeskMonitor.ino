#include "Arduino_SensorKit.h"

int LIGHT_SENSOR = A3;
unsigned long TEN_MINUTES = 600000;
// unsigned long SECONDS = 1000;

void setup()
{
    Serial.begin(115200);
    while (!Serial)
        ;
    Oled.begin();
    Oled.setFlipMode(true);
    Environment.begin();
}

void loop()
{
    Oled.setFont(u8x8_font_chroma48medium8_r);
    Oled.setFont(u8x8_font_7x14B_1x2_f);
    Oled.setCursor(0, 33); // Set the Coordinates
    Oled.print("TempE: ");
    Oled.print(String(Environment.readTemperature())); // Print the Values
    Oled.print(" C ");
    Oled.print("\n");
    Oled.print("Hum:  ");
    Oled.print(String(Environment.readHumidity()));
    Oled.print(" %");
    char sep = ',';
    Serial.println(String() + analogRead(LIGHT_SENSOR) + sep + Environment.readTemperature() \ 
        + sep + Environment.readHumidity());
    Oled.refreshDisplay();
    delay(TEN_MINUTES);
}
