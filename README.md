# Desktop Monitor

## Description

Simple program to monitor my desk during vacations and log sensors into a sqlite database on a Raspberry Pi 
 using the [Arduino Uno Sensorkit](https://sensorkit.arduino.cc/). Raw light, temperature (°C) and humidity (%) are 
 recorded by the arduino, id and timestamps are added by [`sqlite_logger.py`](sqlite_logger.py). `sensors_data` table
 in `sensorkit.db` file looks like this :

| id | light | temperature | humidity | date                |
|:---|:-----:|:------------|:---------|:--------------------|
| 1  |  503  | 28.2        | 36.0     | 2022-07-13 13:46:03 |
| 2  |  504  | 28.1        | 35.0     | 2022-07-13 13:46:08 |
| 3  |  507  | 28.2        | 36.0     | 2022-07-13 13:46:13 |

## Build and run

I use [`arduino-cli`](https://github.com/arduino/arduino-cli) and the following script to compile and upload
  as `build.sh`:

```bash
#!/bin/bash

port=$(find /dev -name ttyACM*)

arduino-cli compile --fqbn arduino:avr:uno $1
arduino-cli upload -p $port --fqbn arduino:avr:uno $1
```

And then run :

    ./build.sh DesktopMonitor

To start the database insertions (<kbd>Ctrl</kbd> + <kbd>C</kbd> to stop):

    ./sqlite_logger.py

To view the database 

    $ sqlite3 sensorkit.db
    .headers ON
    sqlite> SELECT * FROM sensors_data;
    id|light|temperature|humidity|date
    1|503|28.2|36.0|2022-07-13 13:46:03
    2|504|28.1|35.0|2022-07-13 13:46:08
    3|507|28.2|36.0|2022-07-13 13:46:13

## Requirements

- [ArduinoSensorkit](https://www.arduino.cc/reference/en/libraries/arduino_sensorkit/)
- Python3
- sqlite3
